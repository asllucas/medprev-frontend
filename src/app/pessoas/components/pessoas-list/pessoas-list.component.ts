import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Pessoas } from '../../models/pessoas';

@Component({
  selector: 'app-pessoas-list',
  templateUrl: './pessoas-list.component.html',
  styleUrls: ['./pessoas-list.component.scss']
})
export class PessoasListComponent implements OnInit {

 @Input() pessoas: Pessoas[];
 @Output() editar = new EventEmitter<number>();
 @Output() remover = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
    
  }

}
