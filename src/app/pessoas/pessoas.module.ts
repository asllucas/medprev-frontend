import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PessoasListComponent } from "./components/pessoas-list/pessoas-list.component";
import { PessoasListagemComponent } from "./containers/pessoas-listagem/pessoas-listagem.component";
import { PessoasRoutingModule } from "./pessoas-routing.module";
import { PessoasFormComponent } from './containers/pessoas-form/pessoas-form.component';
import { CompartilhadoModule } from "../compartilhados/compartilhados.module";

@NgModule({
    declarations: [
        PessoasListagemComponent,
        PessoasListComponent,
        PessoasFormComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        PessoasRoutingModule,
        CompartilhadoModule
    ],
    exports: [
        PessoasListComponent
    ]
})
export class PessoasModule {

}