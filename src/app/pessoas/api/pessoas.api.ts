import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { Pessoas } from "../models/pessoas";

@Injectable({ providedIn: 'root' })
export class PessoasApi {

    private API_URL = environment.API_URL;

    constructor(private http: HttpClient){}

    public getAllPessoas(): Observable<Pessoas[]> {
        return this.http.get<Pessoas[]>(`${this.API_URL}/pessoas`).pipe(
            catchError(this.errorHandler)
        );
    }

    savePessoa(pessoa: Pessoas): Observable<Pessoas> {
        return this.http.post<Pessoas>(`${this.API_URL}/pessoas`, pessoa);
    }

    deletePessoa(id: number): Observable<Pessoas> {
        return this.http.delete<Pessoas>(`${this.API_URL}/pessoas/${id}`).pipe(
            catchError(this.errorHandler)
        );
    }

    editPessoa(id: number, pessoa: Pessoas): Observable<Pessoas> {
        return this.http.put<Pessoas>(`${this.API_URL}/pessoas/${id}`, pessoa);
    }

    getById(id: number): Observable<Pessoas> {
        return this.http.get<Pessoas>(`${this.API_URL}/pessoas/${id}`);
    }

    errorHandler(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        } else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }

}