export class Pessoas {
    tipo: string;
    nome: string;
    razao_social: string;
    cpf: string;
    cnpj: string;
    sexo: string;
    data_nascimento: string;
    email: string;
    telefone: string;
    celular: string;
    foto: string;
    endereco: string;
    numero: string;
    complemento: string;
    bairro: string;
    cep: string;
    cidade: string;
    uf: string;
}