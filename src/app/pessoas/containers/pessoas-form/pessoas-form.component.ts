import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PessoasApi } from '../../api/pessoas.api';

@Component({
  selector: 'app-pessoas-form',
  templateUrl: './pessoas-form.component.html',
  styleUrls: ['./pessoas-form.component.scss']
})
export class PessoasFormComponent implements OnInit {

  formPessoa: FormGroup
  formSubmitted = false;

  constructor(private form: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private api: PessoasApi) { }

  ngOnInit(): void {
    this.iniciarForm();
  }

  private iniciarForm() {
    this.formPessoa = this.form.group({
      tipo: ['', [Validators.required]],
      nome: ['', [Validators.required]],
      razao_social: ['', [Validators.required]],
      cpf: ['', [Validators.required]],
      cnpj: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
      data_nascimento: ['', [Validators.required]],
      email: [''],
      telefone: [''],
      celular: [''],
      foto: [''],
      endereco: ['', [Validators.required]],
      numero: ['', [Validators.required]],
      complemento: [''],
      bairro: [''],
      cep: ['', Validators.required],
      cidade: ['', Validators.required],
      uf: ['', [Validators.required]]
    });
  }

  submit() {

  }

}
