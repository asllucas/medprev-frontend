import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PessoasApi } from '../../api/pessoas.api';
import { Pessoas } from '../../models/pessoas';

@Component({
  selector: 'app-pessoas-listagem',
  templateUrl: './pessoas-listagem.component.html',
  styleUrls: ['./pessoas-listagem.component.scss']
})
export class PessoasListagemComponent implements OnInit {

  pessoas: Pessoas[];

  constructor(private api: PessoasApi,
    private router: Router) { }

  ngOnInit(): void {
    this.api.getAllPessoas().subscribe((response: Pessoas[]) => {
      console.log(response);
      this.pessoas = response
    });
  }

  public async acessarRotaCadastro() {
    this.router.navigate([`pessoas/cadastro`]);
  }

  public acessarRotaEdicao(id: number): void {
    this.router.navigate([`pessoas/${id}/alterar`]);
  }

}
